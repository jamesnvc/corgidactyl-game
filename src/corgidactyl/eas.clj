(ns corgidactyl.eas
  (:require
    [clojure.set :as set]
    [clojure.spec :as s]))

;; Entity-Aspect-System thing
; Inspired by Steve Losh's BEAST
; https://bitbucket.org/sjl/beast

;; Common specs

(s/def ::slot-specifier
  (s/or :just-name keyword?
        :name-and-default
        (s/cat :name keyword? :default (constantly true))))

(s/def ::slot-specifiers
  (s/* ::slot-specifier))

(defn normalize-slots
  "Turn a list of `::slot-specifier`s to a map of name to default or nil"
  [aspect slots]
  (let [keywordize (fn [k]
                     (if (some? (namespace k))
                       k
                       (keyword aspect (name k))))]
    (reduce (fn [acc [n v]]
              (case n
                :just-name
                (assoc acc (keywordize v) nil)
                :name-and-default
                (assoc acc (keywordize (:name v)) (:default v))))
            {}
            slots)))

;; Aspects

(defonce aspects (atom {}))

(defmacro defaspect
  "Create an aspect. Aspects have a name and slots (which are keywords).
  Slots are automatically prefixed with the name of the aspect.
  e.g.,

  (defaspect location
    (:x 0) (:y 0) :z)

  Would define an aspect where the :location/x & :location/y slots will default
  to zero and the :z slot will default to nil.
  It will also create a `location?` function that can determine if an entity
  implements the aspect"
  [aspect-name & specifiers]
  (s/assert ::slot-specifiers specifiers)
  (let [slots (->> specifiers
                   (s/conform ::slot-specifiers)
                   (normalize-slots (str aspect-name)))]
    `(do
       (swap! aspects assoc (quote ~aspect-name) ~slots)
       (defn ~(symbol (str aspect-name "?"))
         [x#]
         (set/subset? ~(set (keys slots)) (set (keys x#)))))))

;; Entities

(s/def ::entity-aspects
  (s/* symbol))

(defn entity-type
  [e]
  (::type e))

(defmacro defentity
  "Define an entity that implements the given aspects and optionally some slots.
  This will create an `entity-name?` function to determine if a given entity is
  an instance of `entity-name` and a `make-entity-name` that will create an
  instance of the entity.
  An initializer can be supplied under the key `:init` which will be passed the
  entity map & can return a new map"
  [entity-name impl-aspects & slots]
  (s/assert ::entity-aspects impl-aspects)
  (s/assert ::slot-specifiers slots)
  (let [slots (->> slots
                   (s/conform ::slot-specifiers)
                   ; Entity slot names don't get a prefix
                   (normalize-slots nil))
        slots (assoc slots ::type (keyword entity-name))
        entity-aspects (s/conform ::entity-aspects impl-aspects)]
    ; TODO: calculate combined-slots at runtime so it takes into account
    ; changes in aspects?
    `(let [combined-slots# (reduce (fn [s# asp#] (merge (get @aspects asp#) s#))
                                   ~slots
                                   (quote ~entity-aspects))]
       (defn ~(symbol (str entity-name "?"))
         [e#]
         (set/subset? (set (keys combined-slots#)) (set (keys e#))))

       (defn ~(symbol (str "make-" entity-name))
         []
         (let [slots# combined-slots#]
           ((get slots# :init identity) slots#))))))

;; Systems

(s/def ::entity-specifier
  (s/or
    :just-name symbol?
    :name-and-aspects
    (s/cat :entity-name symbol?
           :entity-aspects (s/* symbol?))))

(s/def ::entity-specifiers
  (s/+ ::entity-specifier))

(defmacro defsystem
  "Define a system that operates on the entity or entities matching the given
  specifiers and the current context.
  The first argument will always be the context
  e.g.

  (defsystem move [ctx [entity location movable]]
    (let [new-loc ((:movable/move-fn entity) ctx entity)]
      (merge entity new-loc)))

  If the run-system function is invoked without an entities, it returns a transducer"
  [system-name [ctx-name & entity-specifiers] & body]
  (s/assert ::entity-specifiers entity-specifiers)
  (let [e-specs (s/conform ::entity-specifiers entity-specifiers)
        arg-name (fn [[n v]]
                   (case n
                     :just-name v
                     :name-and-aspects (:entity-name v)))
        arg-names (map arg-name e-specs)
        arg-predicates (map
                         (fn [[n v]]
                           (case n
                             :just-name `(constantly true)
                             :name-and-aspects
                             `(fn [e#]
                                (every?
                                  (fn [p#] (p# e#))
                                  ~(mapv
                                     (fn [aspect-name]
                                       (symbol (str aspect-name "?")))
                                     (:entity-aspects v))))))
                         e-specs)
        entities (gensym "entities")
        run-body (if (= 1 (count arg-names))
                   `(map
                      (fn [e#] (if (~(first arg-predicates) e#)
                                 (let [~(first arg-names) e#]
                                   ~@body)
                                 e#)))
                   (assert false "Multi-arg systems not implemented yet")) ]
    `(do
       (defn ~(symbol (str "run-" system-name))
         ([~ctx-name] ~run-body)
         ([~ctx-name ~entities]
          (into [] ~run-body ~entities))))))

(comment

  (defaspect location
    (:x 0)
    (:y 0))

  (defaspect movable
    (:move-fn (fn [ctx e] (select-keys e [:location/x :location/y]))))

  (defentity ptero [location movable]
    (:speed 0.1)
    (:movable/move-fn (fn [ctx p]
                        (-> p
                            (select-keys [:location/x :location/y])
                            (update :location/x inc)
                            (update :location/y inc)))))

  (defsystem move [ctx [entity location movable]]
    (let [new-loc ((:movable/move-fn entity) ctx entity)]
      (merge entity new-loc))))
